﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using FurnitureAPI.Models;

namespace FurnitureAPI.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class OtherController : ControllerBase
    {
        private readonly OtherContext _context;

        public OtherController(OtherContext context)
        {
            _context = context;
        }

        // GET: api/Other
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Other>>> GetOthers()
        {
            return await _context.Others.ToListAsync();
        }

        // GET: api/Other/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Other>> GetOther(int id)
        {
            var other = await _context.Others.FindAsync(id);

            if (other == null)
            {
                return NotFound();
            }

            return other;
        }

        // PUT: api/Other/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutOther(int id, Other other)
        {
            if (id != other.Id)
            {
                return BadRequest();
            }

            _context.Entry(other).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!OtherExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Other
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<Other>> PostOther(Other other)
        {
            _context.Others.Add(other);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetOther", new { id = other.Id }, other);
        }

        // DELETE: api/Other/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Other>> DeleteOther(int id)
        {
            var other = await _context.Others.FindAsync(id);
            if (other == null)
            {
                return NotFound();
            }

            _context.Others.Remove(other);
            await _context.SaveChangesAsync();

            return other;
        }

        private bool OtherExists(int id)
        {
            return _context.Others.Any(e => e.Id == id);
        }
    }
}
