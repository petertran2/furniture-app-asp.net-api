﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using FurnitureAPI.Models;

namespace FurnitureAPI.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class SofasController : ControllerBase
    {
        private readonly SofaContext _context;

        public SofasController(SofaContext context)
        {
            _context = context;
        }

        // GET: api/Sofas
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Sofa>>> GetSofas()
        {
            return await _context.Sofas.ToListAsync();
        }

        // GET: api/Sofas/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Sofa>> GetSofa(int id)
        {
            var sofa = await _context.Sofas.FindAsync(id);

            if (sofa == null)
            {
                return NotFound();
            }

            return sofa;
        }

        // PUT: api/Sofas/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutSofa(int id, Sofa sofa)
        {
            if (id != sofa.Id)
            {
                return BadRequest();
            }

            _context.Entry(sofa).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SofaExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Sofas
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<Sofa>> PostSofa(Sofa sofa)
        {
            _context.Sofas.Add(sofa);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetSofa", new { id = sofa.Id }, sofa);
        }

        // DELETE: api/Sofas/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Sofa>> DeleteSofa(int id)
        {
            var sofa = await _context.Sofas.FindAsync(id);
            if (sofa == null)
            {
                return NotFound();
            }

            _context.Sofas.Remove(sofa);
            await _context.SaveChangesAsync();

            return sofa;
        }

        private bool SofaExists(int id)
        {
            return _context.Sofas.Any(e => e.Id == id);
        }
    }
}
