﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FurnitureAPI.Models
{
    public class BedContext : DbContext
    {
        public BedContext(DbContextOptions<BedContext> options) : base(options)
        {

        }

        public DbSet<Bed> Beds { get; set; }
    }
}
