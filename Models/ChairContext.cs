﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FurnitureAPI.Models
{
    public class ChairContext : DbContext
    {
        public ChairContext(DbContextOptions<ChairContext> options) : base(options)
        {

        }

        public DbSet<Chair> Chairs { get; set; }
    }
}
