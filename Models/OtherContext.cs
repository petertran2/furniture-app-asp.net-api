﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FurnitureAPI.Models
{
    public class OtherContext : DbContext
    {
        public OtherContext(DbContextOptions<OtherContext> options) : base(options)
        {

        }

        public DbSet<Other> Others { get; set; }
    }
}
