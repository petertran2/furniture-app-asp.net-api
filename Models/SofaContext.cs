﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FurnitureAPI.Models
{
    public class SofaContext : DbContext
    {
        public SofaContext(DbContextOptions<SofaContext> options) : base(options)
        {

        }

        public DbSet<Sofa> Sofas { get; set; }
    }
}
