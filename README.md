<img align="right" src="https://gitlab.com/petertran2/furniture-app/-/raw/master/README_assets/Peek%202020-02-10%2017-23.gif">

### Furniture App Back-End

This API for furniture products is an ASP.NET Core app that contains listings of different kinds of furniture for an e-commerce shop, ranging from sofas to chairs to beds. Configured as an API, it sends listings data to its corresponding front-end mobile app written in React Native to display the furniture available for sale. It also receives customers' orders from the mobile app and stores them in an in-memory database. The back-end app has been deployed to Azure ([link](https://furnitureapi.azurewebsites.net/)).

For more information about the mobile app, please visit [its repository](https://gitlab.com/petertran2/furniture-app/-/tree/asp-net-version).

### Technologies

- React Native (front end)
- Javascript (ES6+)
- React Navigation
- Vector Icons
- Formik
- ASP.NET Core (back end)
- C#
- Entity Framework
- LINQ
- JSON
- Azure

### Author

Peter Tran